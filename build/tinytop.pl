spo('ObjectProperty',type,'Property').
spo('AnnotationProperty',type,'Property').
spo('DatatypeProperty',type,'Property').
spo('OntologyProperty',type,'Property').
spo(subClassOf,type,'Property').
spo(subClassOf,type,'ReflexiveProperty').
spo(subClassOf,type,'TransitiveProperty').
spo(subClassOf,label,"subclass of").
spo(subClassOf,comment,"A subClassOf B means A subsumes B").
spo(disjointWith,type,'Property').
spo(disjointWith,type,'SymmetricProperty').
spo(disjointWith,label,"disjoint with").
spo(disjointWith,comment,"A disjoint with B means nothing can be an instance of A and B").
spo(comment,type,'AnnotationProperty').
spo(comment,range,'String').
spo(comment,label,"comment").
spo(comment,comment,"A comment B means B describes A").
spo(label,type,'AnnotationProperty').
spo(label,range,'String').
spo(label,label,"label").
spo(label,comment,"A label B means B is a human readable form of the ID: A.").
spo('Thing',type,'Class').
spo('Thing',label,"Thing").
spo('Thing',comment,"Thing is the top class that subsumes everything").
spo('Nothing',type,'Class').
spo('Nothing',label,"Nothing").
spo('Nothing',comment,"Nothing is the bottom class that has no member instances").
spo('Continuant',type,'Class').
spo('Continuant',label,"Continuant").
spo('Continuant',comment,"A continuant is something that when it exists it exists in its entirety and continues to persist through time").
spo('Continuant',subClassOf,'Thing').
spo('Continuant',disjointWith,'Occurant').
spo('Occurant',type,'Class').
spo('Occurant',label,"Occurant").
spo('Occurant',comment,"An Occurant is something that when it exists it only exists in part at any particular time").
spo('Occurant',subClassOf,'Thing').
spo('Occurant',disjointWith,'Continuant').
spo('Process',type,'Class').
spo('Process',label,"Process").
spo('Process',comment,"A sequence of events, including the empty sequence").
spo('Process',subClassOf,'Process').
spo('Capability',type,'Class').
spo('Capability',label,"Capability").
spo('Capability',comment,"A Capability is an Entity's ability to perform some action. It is distinct from actual doing of the action, which would be a Process. Instead it denotes that the Entity can do such an action").
spo('Capability',subClassOf,'Continuant').
spo('Entity',type,'Class').
spo('Entity',label,"Entity").
spo('Entity',comment,"An Entity is a continuant that is not dependent on any other thing for its existence.").
spo('Entity',subClassOf,'Continuant').
spo('Entity',disjointWith,'Quality').
spo('InformationEntity',type,'Class').
spo('InformationEntity',label,"Information Entity").
spo('InformationEntity',comment,"An information entity is a carrier for some particular information, such as a book, an audio recording, or http packet.").
spo('InformationEntity',subClassOf,'Continuant').
spo('Quality',type,'Class').
spo('Quality',label,"Quality").
spo('Quality',comment,"Descriptor of some attribute, quality or characteristic of some Entity").
spo('Quality',subClassOf,'Continuant').
spo(topObjectProperty,type,'ObjectProperty').
spo(topObjectProperty,label,"top object property").
spo(topObjectProperty,comment,"The top property which subsumes all others").
spo(topObjectProperty,domain,'Thing').
spo(topObjectProperty,range,'Thing').
spo(domain,type,'Property').
spo(domain,label,"domain").
spo(domain,comment,"The acceptable type for the domain of a relation").
spo(domain,domain,'Property').
spo(domain,range,'Thing').
spo(range,type,'Property').
spo(range,label,"domain").
spo(range,comment,"The acceptable type for the range of a relation").
spo(range,domain,'Property').
spo(range,range,'Thing').
spo(inverseOf,type,'Property').
spo(inverseOf,type,'SymmetricProperty').
spo(inverseOf,label,"inverse of").
spo(inverseOf,comment,"A inverse of B means if X A Y holds then Y B X also holds").
spo(inverseOf,domain,'ObjectProperty').
spo(inverseOf,range,'ObjectProperty').
spo(capabilityOf,type,'ObjectProperty').
spo(capabilityOf,subPropertyOf,topObjectProperty).
spo(capabilityOf,label,"capability of").
spo(capabilityOf,comment,"Capability capability of Entity means the Entity can realize that Capability in a Process").
spo(capabilityOf,domain,'Capability').
spo(capabilityOf,range,'Entity').
spo(capabilityOf,inverseOf,hasCapability).
spo(hasCapability,type,'ObjectProperty').
spo(hasCapability,subPropertyOf,topObjectProperty).
spo(hasCapability,label,"has capability").
spo(hasCapability,comment,"Entity has capability Capability means the Entity can realize that Capability in a Process").
spo(hasCapability,domain,'Entity').
spo(hasCapability,range,'Capability').
spo(hasCapability,inverseOf,capabilityOf).
spo(hasParticipant,type,'ObjectProperty').
spo(hasParticipant,subPropertyOf,topObjectProperty).
spo(hasParticipant,label,"has participant").
spo(hasParticipant,comment,"Process has participant Entity means the Entity somehow participates in the Process").
spo(hasParticipant,domain,'Process').
spo(hasParticipant,range,'Entity').
spo(hasParticipant,inverseOf,participatesIn).
spo(participatesIn,type,'ObjectProperty').
spo(participatesIn,subPropertyOf,topObjectProperty).
spo(participatesIn,label,"participates in").
spo(participatesIn,comment,"Entity participates in Process means the Entity somehow participates in the Process").
spo(participatesIn,domain,'Entity').
spo(participatesIn,range,'Process').
spo(participatesIn,inverseOf,hasParticipant).
spo(hasProcessPart,type,'ObjectProperty').
spo(hasProcessPart,type,'TransitiveProperty').
spo(hasProcessPart,subPropertyOf,topObjectProperty).
spo(hasProcessPart,label,"has process part").
spo(hasProcessPart,comment,"A has process part B means process B occurs entirely within process A as part of it").
spo(hasProcessPart,domain,'Process').
spo(hasProcessPart,range,'Process').
spo(hasProcessPart,inverseOf,isProcessPartOf).
spo(isProcessPartOf,type,'ObjectProperty').
spo(isProcessPartOf,type,'TransitiveProperty').
spo(isProcessPartOf,subPropertyOf,topObjectProperty).
spo(isProcessPartOf,label,"has process part").
spo(isProcessPartOf,comment,"B is process part of A means process B occurs entirely within process A as part of it").
spo(isProcessPartOf,domain,'Process').
spo(isProcessPartOf,range,'Process').
spo(isProcessPartOf,inverseOf,hasProcessPart).
spo(hasDirectProcessPart,type,'ObjectProperty').
spo(hasDirectProcessPart,subPropertyOf,hasProcessPart).
spo(hasDirectProcessPart,label,"has direct process part").
spo(hasDirectProcessPart,comment,"A has direct process part B means process B occurs entirely within process A as part of it, with no transitivity").
spo(hasDirectProcessPart,domain,'Process').
spo(hasDirectProcessPart,range,'Process').
spo(hasDirectProcessPart,inverseOf,isDirectProcessPartOf).
spo(isDirectProcessPartOf,type,'ObjectProperty').
spo(isDirectProcessPartOf,subPropertyOf,isProcessPartOf).
spo(isDirectProcessPartOf,label,"has process part").
spo(isDirectProcessPartOf,comment,"B is process part of A means process B occurs entirely within process A as part of it, with no transitivity").
spo(isDirectProcessPartOf,domain,'Process').
spo(isDirectProcessPartOf,range,'Process').
spo(isDirectProcessPartOf,inverseOf,hasDirectProcessPart).
spo(hasQuality,type,'ObjectProperty').
spo(hasQuality,subPropertyOf,topObjectProperty).
spo(hasQuality,label,"has quality").
spo(hasQuality,comment,"Entity has quality Quality means the Entity is described by that Quality").
spo(hasQuality,domain,'Entity').
spo(hasQuality,range,'Quality').
spo(hasQuality,inverseOf,isQualityOf).
spo(isQualityOf,type,'ObjectProperty').
spo(isQualityOf,subPropertyOf,topObjectProperty).
spo(isQualityOf,label,"has quality").
spo(isQualityOf,comment,"Quality is quality of Entity means the Entity is described by that Quality").
spo(isQualityOf,domain,'Quality').
spo(isQualityOf,range,'Entity').
spo(isQualityOf,inverseOf,hasQuality).
spo(informedBy,type,'ObjectProperty').
spo(informedBy,subPropertyOf,topObjectProperty).
spo(informedBy,label,"is informed by").
spo(informedBy,comment,"Entity is informed by information entity means the Entity can make use of information recorded in the information entity").
spo(informedBy,domain,'Entity').
spo(informedBy,range,'InformationEntity').
spo(isProceededBy,type,'ObjectProperty').
spo(isProceededBy,type,'TransitiveProperty').
spo(isProceededBy,subPropertyOf,topObjectProperty).
spo(isProceededBy,label,"is proceeded by").
spo(isProceededBy,comment,"A is proceeded by B means B occurs before A").
spo(isProceededBy,domain,'Process').
spo(isProceededBy,range,'Process').
spo(isProceededBy,inverseOf,precedes).
spo(precedes,type,'ObjectProperty').
spo(precedes,type,'TransitiveProperty').
spo(precedes,subPropertyOf,topObjectProperty).
spo(precedes,label,"precedes").
spo(precedes,comment,"B precedes A means B occurs before A").
spo(precedes,domain,'Process').
spo(precedes,range,'Process').
spo(precedes,inverseOf,isProceededBy).
spo(isImmediatelyProceededBy,type,'ObjectProperty').
spo(isImmediatelyProceededBy,subPropertyOf,isProceededBy).
spo(isImmediatelyProceededBy,label,"is immediately proceeded by").
spo(isImmediatelyProceededBy,comment,"A is immediately proceeded by B means B immediately occurs before A and no process occurs between them").
spo(isImmediatelyProceededBy,domain,'Process').
spo(isImmediatelyProceededBy,range,'Process').
spo(isImmediatelyProceededBy,inverseOf,immediatelyPrecedes).
spo(immediatelyPrecedes,type,'ObjectProperty').
spo(immediatelyPrecedes,subPropertyOf,precedes).
spo(immediatelyPrecedes,label,"immediately precedes").
spo(immediatelyPrecedes,comment,"B precedes A means B immediately occurs before A and no process occurs between them").
spo(immediatelyPrecedes,domain,'Process').
spo(immediatelyPrecedes,range,'Process').
spo(immediatelyPrecedes,inverseOf,isImmediatelyProceededBy).
