
:- use_module(frame_compiler,
    [ compile/2
	]
).

:- prolog_load_context(directory, Dir),
   asserta(user:file_search_path(root, Dir)),
   asserta(user:file_search_path(src, root(src))),
   asserta(user:file_search_path(build, root(build))).

:- dynamic compiled/0.

compile_all :-
	forall(requires_compiling(F),
	    ( build_file_path(F, BP),
		  F = file(N, _, FP),
		  compile(FP, BP),
		  format("compiled ~w~n", N),
		  (compiled ; asserta(compiled))
	    )).

trim_all :-
	forall(requires_triming(F, P), (delete_file(P), format("cleaned ~w~n", F))).

make_object :-
	absolute_file_name(src(obj_head), Head),
	format(atom(Command1), "cat ~w > kb.lgt", Head),
	shell(Command1, 0),
	shell('cat build/*.pl >> kb.lgt', 0),
	shell('echo ":- end_object." >> kb.lgt', 0),
	writeln('compiled kb.lgt').

make_module :-
	absolute_file_name(src(mod_head), Head),
	format(atom(Command1), "cat ~w > kb.pl", Head),
	shell(Command1, 0),
	shell('cat build/*.pl >> kb.pl', 0),
	writeln('compiled kb.pl').

required_directories :-
	absolute_file_name(src(.), SRC),
	absolute_file_name(build(.), BUILD),
    exists_dir(SRC),
    exists_or_make_dir(BUILD).

exists_dir(Dir) :-
	exists_directory(Dir), !.
exists_dir(Dir) :-
   throw(error(existence_error(source_sink, Dir), context(required_directories/0, 'No ./src or ./build directories'))).

exists_or_make_dir(Dir) :-
	( exists_directory(Dir)
	; make_directory(Dir)
	), !.
exists_or_make_dir(Dir) :-
   throw(error(existence_error(source_sink, Dir), context(required_directories/0, 'No ./src or ./build directories'))).

dir_file(Dir, file(File, Name, Extension, Time, FilePath)) :-
	ROOT =.. [Dir, '.'],
	absolute_file_name(ROOT, Path),
    directory_files(Path, Files),
	maplist(dif(File), ['..', '.']),
	member(File, Files),
	file_name_extension(Name, Extension, File),
	FileSpec =.. [Dir, File],
	absolute_file_name(FileSpec, FilePath),
	time_file(FilePath, Time).

requires_compiling(file(File, Name, Path)) :-
	dir_file(src, file(File, Name, frames, T1, Path)),
	( \+ dir_file(build, file(_, Name, pl, _, _))
	; dir_file(build, file(_, Name, pl, T2, _)), T1 > T2
	).

requires_triming(F, Path) :-
	dir_file(build, file(F, Name, pl, _, Path)),
	\+ dir_file(src, file(_, Name, frames, _, _)).

build_file_path(file(_, Name, _), Build) :-
	file_name_extension(Name, pl, FileName),
	absolute_file_name(build(FileName), Build).


:- required_directories,
	trim_all,
	compile_all,
	(compiled, make_object, make_module ; true ),
	halt.

