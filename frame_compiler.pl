:- module(frame_compiler,
	[ compile/2
	]).

/** Frame Compiler
*  Take an input file of frames and transpose
*  to a Prolog file of triples.
*
*  Use in workflow: init(compile -> expand_term to object)
*/

:- use_module(library(dcg/basics),
	[ string_without//2
	, string//1
	, blanks//0
	, nonblanks//1
	, white//0
	, eos//0
	]).
:- use_module(library(pure_input),
	[ phrase_from_file/2
	, syntax_error//1
	]).

compile(FileIn, FileOut) :-
	phrase_from_file(frames(FrCodes), FileIn),
	flatten(FrCodes, FlatCodes),
	maplist(code_slot, FlatCodes, Frames),
	setup_call_cleanup(
		open(FileOut, 'write', Stream),
		maplist(format(Stream, "~q.~n"), Frames),
		close(Stream)
	), !.

code_slot(spo(SC, KC, str(VC)), spo(S, K, V)) :- % 34 = '"'
	atom_codes(S, SC),
	atom_codes(K, KC),
	string_codes(V, VC).
code_slot(spo(SC, KC, VC), spo(S, K, V)) :-
	VC \= str(_),
	maplist(atom_codes, [S, K, V], [SC, KC, VC]).

frames([]) -->
	blanks.
frames([H|T]) -->
	frame(H),
	either(nl, eos), blanks, % 10 = "\n"
	frames(T).

frame(Pairs) -->
	subject(S),
	blanks,
	pairs(S, Pairs).

subject(S) -->
	string(S),
	maybe(nl),
	white.

pairs(_, []) -->
	[].
pairs(S, [P|T]) -->
	pair(S, P),
	pairs(S, T).

pair(S, spo(S, K, V)) -->
	blanks,
	nonblanks(K),
	white,
	str_atom(V),
	{ V \= [] },
	nl.
pair(S, _) --> {atom_codes(SS, S)},
	syntax_error('Could\'t determine key value pairs of frame'-SS).

nl --> [10].
nl --> [13, 10].

str_atom(str(V)) -->
    [34], string(V), [34], {\+ member(10, V)}.
str_atom(V) -->
    nonblanks(V), {\+ member(34, V), \+ member(10, V)}.
str_atom(_) -->
	syntax_error('Could\'t determine value').

maybe(F) --> F ; [].
either(A, B) --> A ; B.
