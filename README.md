# Frame Compiler

This is a little utility tool I created to help make it easier to maintain an
ontology in Prolog. It takes a bunch of "frames" files and creates a SWI-Prolog
module of `spo/3` predicates and a Logtalk object with the same public
predicates.

## Frames syntax

```
subject pred1 value1
	pred2 value2

```

Similar to Turtle but not as clever.
The first line of a frame can be a triple, or just the subject. The following
slots of predicate-value pairs need to be indented to show they belong to the
frame. A blank line terminates the frame. Rules and slot types are not
supported.

## How to use

Add files to the `src` directory with a `.frames` extension in the frames
syntax. You can also edit the `obj_head` and `mod_head` files there as
appropriate.

To compile run:

```
swipl -s make.pl
```

## Quirks

There's a couple of quirks that would be nice to iron out that I choose to live
with because this is just a personal tool and in the balance of time vs
productivity they loose.

So if you edit a `*_head` file or remove a frames file it won't recompile the
`kb.{pl, lgt}` files. To fix just delete the files in the build directory and
recompile.
